require "./random/base36"

n = ARGV.empty? ? nil : ARGV.first.to_i

puts Random.base36(n)
