require "random"

module Random
  BASE36_ALPHABET =	("0".."9").to_a + ("a".."z").to_a

  # https://api.rubyonrails.org/classes/SecureRandom.html#method-c-base36
  def self.base36(n : Int | Nil = 16) : String
    Random.new.random_bytes(n || 16).map do |byte|
      idx = byte % 64
      idx = Random.rand(36) if idx >= 36

      BASE36_ALPHABET[idx]
    end.join
  end
end
