# base36

A Base36 port of Rails' SecureRandom.base36 used by ActiveStorage

## Usage

As a command:

```sh
base36 28 # default length is 16
```

As a shard:

```crystal
require "random/base36"

Random.base36(28)
```

## Contributing

1. Fork it (<https://0xacab.org/sutty/base36/fork>)
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request

## Contributors

- [fauno](https://0xacab.org/fauno) - maintainer
